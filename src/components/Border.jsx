import React from "react";

class Border extends React.Component {
  render() {
    return (
      <div className="Border">
        <h1>{this.props.title}</h1>
        <div>{this.props.children}</div>
      </div>
    );
  }
}

export default Border;
